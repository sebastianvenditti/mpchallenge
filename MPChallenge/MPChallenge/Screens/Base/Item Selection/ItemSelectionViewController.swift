//
//  ItemSelectionViewController.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit
import PKHUD

/**
 Base class that handles displaying a list of items allows the user to pick one.
 */
class ItemSelectionViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel : ItemSelectionViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.loadItems()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ItemSelectionViewController {
            destination.viewModel.paymentTransaction = self.viewModel.paymentTransaction
        }
    }
    
    func configureViewModel() {
        viewModel.onLoading = { [weak self] in
            guard let strongSelf = self else {return}
            HUD.show(.progress, onView: strongSelf.view)
        }
        
        viewModel.onSuccess = { [weak self] in
            guard let strongSelf = self else {return}
            strongSelf.tableView.reloadData()
            HUD.flash(.success, onView: strongSelf.view, delay: 0.5)
        }
        
        viewModel.onError = { [weak self] (errorMessage) in
            guard let strongSelf = self else {return}
            HUD.hide()
            strongSelf.showMessage(errorMessage, title: "¡Oh, no!", okAction: {
                strongSelf.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    // MARK: Template methods (should be overridden by subclasses)
    
    func onItemSelected(_ item : ListItem) { }
    
    func cellIdentifier() -> String { return "" }
    
    func configure(cell: UITableViewCell, with item: ListItem) {}
    
}


// MARK: UITableViewDataSource

extension ItemSelectionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier(), for: indexPath)
        
        guard indexPath.row < self.viewModel.items.count else { return cell }
        
        let currentItem = self.viewModel.items[indexPath.row]
        self.configure(cell: cell, with: currentItem)
        
        return cell
    }
}


// MARK: UITableViewDelegate

extension ItemSelectionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row < self.viewModel.items.count {
            let selectedItem = self.viewModel.items[indexPath.row]
            self.onItemSelected(selectedItem)
        }
    }
}
