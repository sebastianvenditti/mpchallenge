//
//  ItemSelectionViewModel.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit

/**
 Base class that handles the logic and internal state of a list of items.
 */
class ItemSelectionViewModel: NSObject {
    
    var paymentTransaction : PaymentTransaction?
    
    var items : [ListItem] = []
    
    var onLoading: (()->())?
    var onSuccess: (()->())?
    var onError: ((String)->())?
    
    
    // MARK: Template methods (should be overridden by subclasses)
    
    func loadItems() { }
}
