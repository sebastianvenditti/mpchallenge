//
//  CardIssuerSelectionViewController.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit
import AlamofireImage

/**
 Screen where the user selects the card issuer.
 */
class CardIssuerSelectionViewController: ItemSelectionViewController {
    
    let payerCostSelectionScreenSegue = "PayerCostSelectionScreenSegue"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        viewModel = CardIssuerSelectionViewModel()
    }
    
    override func cellIdentifier() -> String {
        return "CardIssuerCell"
    }
    
    override func onItemSelected(_ item : ListItem) {
        if let selectedCardIssuer  = item as? CardIssuer {
            self.viewModel.paymentTransaction?.cardIssuer = selectedCardIssuer
            self.performSegue(withIdentifier: payerCostSelectionScreenSegue, sender: self)
        }
    }
    
    override func configure(cell: UITableViewCell, with item: ListItem) {
        if let cardIssuerCell = cell as? CardIssuerTableViewCell,
            let cardIssuer = item as? CardIssuer
        {
            cardIssuerCell.nameLabel?.text = "\(cardIssuer.name ?? "")"
            if let thumbnailURL = cardIssuer.thumbnail {
                cardIssuerCell.thumbnailImageView.image = nil
                cardIssuerCell.thumbnailImageView?.af_setImage(withURL: thumbnailURL)
            }
        }
    }
}
