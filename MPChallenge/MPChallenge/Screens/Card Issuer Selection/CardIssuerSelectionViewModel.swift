//
//  CardIssuerSelectionViewModel.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Alamofire
import Alamofire_Gloss

/**
 View model that handles the logic and internal state of the screen where the user selects the card issuer.
 */
class CardIssuerSelectionViewModel: ItemSelectionViewModel {
    
    override func loadItems() {
        
        if let paymentMethodId = self.paymentTransaction?.paymentMethod?.id {
            onLoading?()
            
            Alamofire.request(APIHelper.url("payment_methods/card_issuers?payment_method_id=\(paymentMethodId)")).responseArray(CardIssuer.self) { [weak self] (response) in
                
                guard let strongSelf = self else {return}
                
                switch response.result {
                    
                case .success(let cardIssuers):
                    strongSelf.items = cardIssuers
                    strongSelf.onSuccess?()
                    
                case .failure(let error):
                    print(error)
                    strongSelf.onError?("No se pudo cargar la lista de emisores de tarjeta... \n\nPor favor, volvé a intentarlo.")
                }
            }
        }
    }
}
