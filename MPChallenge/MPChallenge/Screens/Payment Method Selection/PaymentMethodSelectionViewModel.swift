//
//  PaymentMethodSelectionViewModel.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Alamofire
import Alamofire_Gloss

/**
 View model that handles the logic and internal state of the screen where the user selects the payment method.
 */
class PaymentMethodSelectionViewModel: ItemSelectionViewModel {
    
    override func loadItems() {
        onLoading?()
        
        Alamofire.request(APIHelper.url("payment_methods")).responseArray(PaymentMethod.self) { [weak self] (response) in
            
            guard let strongSelf = self else {return}
            
            switch response.result {
                
            case .success(let paymentMethods):
                // Use the active payment methods only
                strongSelf.items = paymentMethods.filter { ($0.status ?? "") == "active" }
                strongSelf.onSuccess?()
                
            case .failure(let error):
                print(error)
                strongSelf.onError?("No se pudo cargar la lista de medios de pago... \n\nPor favor, volvé a intentarlo.")
            }
        }
    }
}
