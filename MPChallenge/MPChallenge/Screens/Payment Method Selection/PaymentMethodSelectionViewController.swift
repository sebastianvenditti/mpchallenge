//
//  PaymentMethodSelectionViewController.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit
import AlamofireImage

/**
 Screen where the user selects the payment method.
 */
class PaymentMethodSelectionViewController: ItemSelectionViewController {
    
    let cardIssuerSelectionScreenSegue = "CardIssuerSelectionScreenSegue"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        viewModel = PaymentMethodSelectionViewModel()
    }
    
    override func cellIdentifier() -> String {
        return "PaymentMethodCell"
    }
    
    override func onItemSelected(_ item : ListItem) {
        if let selectedPaymentMethod  = item as? PaymentMethod {
            self.viewModel.paymentTransaction?.paymentMethod = selectedPaymentMethod
            self.performSegue(withIdentifier: cardIssuerSelectionScreenSegue, sender: self)
        }
    }
    
    override func configure(cell: UITableViewCell, with item: ListItem) {
        if let paymentMethodCell = cell as? PaymentMethodTableViewCell,
            let paymentMethod = item as? PaymentMethod
        {
            paymentMethodCell.nameLabel?.text = "\(paymentMethod.name ?? "")"
            if let thumbnailURL = paymentMethod.thumbnail {
                paymentMethodCell.thumbnailImageView.image = nil
                paymentMethodCell.thumbnailImageView?.af_setImage(withURL: thumbnailURL)
            }
        }
    }
}
