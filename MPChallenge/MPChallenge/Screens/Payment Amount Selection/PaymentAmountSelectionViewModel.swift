//
//  PaymentAmountSelectionViewModel.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 08/04/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit

let maximumAmountInCents: Decimal = 1_000_000_00

/**
 View model that handles the logic and internal state of the screen where the user enters the amount to pay.
 */
class PaymentAmountSelectionViewModel: NSObject {
    
    var paymentTransaction : PaymentTransaction?
    
    var onAmountUpdated: ((String)->())?
    
    var amount: Decimal = 0 {
        didSet {
            let formattedAmount = amount.localizedCurrencyString()
            self.onAmountUpdated?(formattedAmount)
        }
    }
    
    func updateAmountIfValid(text: String) {
        if text.isEmpty {
            amount = 0
        } else {
            var temp = text
            temp = temp.replacingOccurrences(of: NSLocale.current.currencySymbol ?? "$", with: "")
            temp = temp.replacingOccurrences(of: ".", with: "")
            temp = temp.replacingOccurrences(of: ",", with: "")
            if let parsedDecimal = Decimal(string: temp) {
                if parsedDecimal <= maximumAmountInCents {
                    // The new value is valid. Update the amount
                    amount = parsedDecimal / 100.0
                }
            }
        }
    }
    
    func maximumAmount() -> String {
        return (maximumAmountInCents / 100.0).localizedCurrencyString()
    }
    
    func paymentTransactionIsComplete() -> Bool {
        if let transaction = self.paymentTransaction {
            if transaction.isComplete() {
                return true
            }
        }
        return false
    }
    
    func paymentTransactionDescription() -> String {
        if let transaction = self.paymentTransaction {
            let amountText = transaction.amount!.localizedCurrencyString()
            return "Monto: \(amountText)\n\nMedio de pago: \(transaction.paymentMethod?.name ?? "")\n\nEntidad: \(transaction.cardIssuer?.name ?? "")\n\nFinanciación: \(transaction.payerCost?.name ?? "")\n"
        }
        return ""
    }
}
