//
//  PaymentAmountSelectionViewController.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit

/**
 Screen where the user enters the amount to pay.
 */
class PaymentAmountSelectionViewController: UIViewController {
    
    let paymentMethodSelectionScreenSegue = "PaymentMethodSelectionScreenSegue"
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var disclaimerLabel: UILabel!
    
    let viewModel = PaymentAmountSelectionViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel.onAmountUpdated = { [weak self] (amount) in
            guard let strongSelf = self else {return}
            strongSelf.amountTextField.text = amount
        }
        
        self.viewModel.amount = 0
        
        self.disclaimerLabel.text = "(máximo: \(viewModel.maximumAmount()))"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if viewModel.paymentTransactionIsComplete() {
            self.showMessage(viewModel.paymentTransactionDescription(), title: "Pago seleccionado:")
            viewModel.paymentTransaction = nil
        } else {
            self.amountTextField.becomeFirstResponder()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == paymentMethodSelectionScreenSegue) {
            let destination = segue.destination as! PaymentMethodSelectionViewController
            self.viewModel.paymentTransaction = PaymentTransaction(amount: self.viewModel.amount)
            destination.viewModel.paymentTransaction = self.viewModel.paymentTransaction
        }
    }
    
    @IBAction func continueButtonAction(_ sender: Any) {
        submit()
    }
    
    // MARK: - Private methods
    
    private func submit() {
        if let amount = amountTextField.text, !amount.isEmpty {
            if viewModel.amount > 0 {
                continuePaymentFlow()
            } else {
                showMessage("Por favor, ingresá un monto válido", okAction: {
                    self.amountTextField.becomeFirstResponder()
                })
            }
        } else {
            showMessage("Por favor, ingresá el monto a pagar", okAction: {
                self.amountTextField.becomeFirstResponder()
            })
        }
    }
    
    private func continuePaymentFlow() {
        self.performSegue(withIdentifier: paymentMethodSelectionScreenSegue, sender: self)
    }
}

// MARK: - UITextFieldDelegate

extension PaymentAmountSelectionViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        amountTextField.resignFirstResponder()
        submit()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            viewModel.updateAmountIfValid(text: updatedText)
        }
        return false
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        viewModel.updateAmountIfValid(text: "")
        return true
    }
}
