//
//  PayerCostSelectionViewModel.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Alamofire
import Alamofire_Gloss

/**
 View model that handles the logic and internal state of the screen where the user selects the number of installments.
 */
class PayerCostSelectionViewModel: ItemSelectionViewModel {
    
    override func loadItems() {
        
        if let paymentMethodId = self.paymentTransaction?.paymentMethod?.id,
            let cardIssuerId = self.paymentTransaction?.cardIssuer?.id,
            let amount = self.paymentTransaction?.amount
        {
            onLoading?()
            
            let formattedAmount = amount.truncatedDecimalString()
            
            Alamofire.request(APIHelper.url("payment_methods/installments?payment_method_id=\(paymentMethodId)&issuer.id=\(cardIssuerId)&amount=\(formattedAmount)")).responseArray(Installments.self) { [weak self] (response) in
                
                guard let strongSelf = self else {return}
                
                switch response.result {
                    
                case .success(let installments):
                    strongSelf.items = installments.first?.payerCosts ?? []
                    strongSelf.onSuccess?()
                    
                case .failure(let error):
                    print(error)
                    strongSelf.onError?("No se pudo cargar la lista de financiaciones... \n\nPor favor, volvé a intentarlo.")
                }
            }
        }
    }
}
