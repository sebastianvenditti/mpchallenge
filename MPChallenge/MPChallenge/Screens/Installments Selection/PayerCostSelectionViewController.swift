//
//  PayerCostSelectionViewController.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit

/**
 Screen where the user selects the number of installments.
 */
class PayerCostSelectionViewController: ItemSelectionViewController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        viewModel = PayerCostSelectionViewModel()
    }
    
    override func cellIdentifier() -> String {
        return "PayerCostCell"
    }
    
    override func onItemSelected(_ item : ListItem) {
        if let selectedPayerCost  = item as? PayerCost {
            self.viewModel.paymentTransaction?.payerCost = selectedPayerCost
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func configure(cell: UITableViewCell, with item: ListItem) {
        if let payerCostCell = cell as? PayerCostTableViewCell,
            let payerCost = item as? PayerCost
        {
            payerCostCell.nameLabel?.text = "\(payerCost.name ?? "")"
        }
    }
}
