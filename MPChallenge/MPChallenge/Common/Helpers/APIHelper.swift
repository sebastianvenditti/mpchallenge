//
//  APIHelper.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit

/**
 Helper class that provides methods to make interaction with the API easier.
 
 In the future, this object may handle authentication tokens storage.
 */
class APIHelper: NSObject {
    
    private static var baseURL = "https://api.mercadopago.com/v1/"
    
    /**
     Returns an absolute URL given a relative path, prefixing the base URL of the API.
     
     - returns:
     A String containing the absolute URL.
     
     - parameters:
        - forPath: the relative path that should be used when constructing the absolute URL.
     
     */
    static func url(_ forPath: String) -> String {
        let separator = forPath.contains("?") ? "&" : "?"
        let publicKeySuffix = "\(separator)public_key=444a9ef5-8a6b-429f-abdf-587639155d88"
        return baseURL + forPath + publicKeySuffix
    }
    
}

