//
//  PaymentMethod.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Foundation
import Gloss

/**
 Represents a payment method object as returned from the API.
 */
struct PaymentMethod: JSONDecodable, ListItem {
    
    let id: String
    let name: String?
    let status: String?
    let thumbnail: URL?
    
    init?(json: JSON) {
        guard let id: String = "id" <~~ json
            else { return nil }
        
        self.id = id
        self.name = "name" <~~ json
        self.status = "status" <~~ json
        self.thumbnail = "secure_thumbnail" <~~ json
    }
}
