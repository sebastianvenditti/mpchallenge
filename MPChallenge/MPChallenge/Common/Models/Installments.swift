//
//  Installments.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Foundation
import Gloss

/**
 Represents an installments object as returned from the API.
 */
class Installments: JSONDecodable {
    
    let payerCosts: [PayerCost]?
    
    required init?(json: JSON) {
        if let payerCosts: [PayerCost] = "payer_costs" <~~ json {
            self.payerCosts = payerCosts
        } else {
            self.payerCosts = []
        }
    }
}
