//
//  PaymentTransaction.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Foundation

/**
 Container object used to group all the information related to a payment transaction.
 */
class PaymentTransaction: NSObject {
    var amount : Decimal?
    var paymentMethod : PaymentMethod?
    var cardIssuer : CardIssuer?
    var payerCost : PayerCost?
    
    init(amount: Decimal?) {
        super.init()
        self.amount = amount
    }
    
    func isComplete() -> Bool {
        return (amount != nil &&
            paymentMethod != nil &&
            cardIssuer != nil &&
            payerCost != nil)
    }
}
