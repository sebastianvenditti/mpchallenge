//
//  ListItemProtocol.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Foundation

/**
 Protocol that represents listable/identifiable items.
 */
protocol ListItem {
    var id: String { get }
    var name: String? { get }
}
