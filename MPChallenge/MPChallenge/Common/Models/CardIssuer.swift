//
//  CardIssuer.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Foundation
import Gloss

/**
 Represents a card issuer object as returned from the API.
 */
class CardIssuer: JSONDecodable, ListItem {
    
    let id: String
    let name: String?
    let thumbnail: URL?
    
    required init?(json: JSON) {
        guard let id: String = "id" <~~ json
            else { return nil }
        
        self.id = id
        self.name = "name" <~~ json
        self.thumbnail = "secure_thumbnail" <~~ json
    }
}
