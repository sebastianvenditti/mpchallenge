//
//  PayerCost.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Foundation
import Gloss

/**
 Represents a payer cost object as returned from the API.
 */
class PayerCost: JSONDecodable, ListItem {
    
    let id: String
    let name: String?
    
    required init?(json: JSON) {
        self.id = ""
        guard let recommended_message: String = "recommended_message" <~~ json
            else { return nil }
        self.name = recommended_message
    }
}
