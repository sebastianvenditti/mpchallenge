//
//  Decimal+Formatting.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import Foundation

/**
 Extension that provides a helper methods to format Decimal numbers easily.
 */
extension Decimal {
    func localizedCurrencyString() -> String {
        return NumberFormatter.localizedString(from: NSDecimalNumber(decimal: self), number: .currency)
    }
    
    func truncatedDecimalString() -> String {
        return String(format: "%.2f", Double(truncating:self as NSNumber))
    }
}
