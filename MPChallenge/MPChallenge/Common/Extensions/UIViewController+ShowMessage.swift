//
//  UIViewController+ShowMessage.swift
//  MPChallenge
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import UIKit

/**
 Extension that provides a helper method to show messages to the user easily.
 */
extension UIViewController {
    func showMessage(_ message: String? = nil,
                     title: String? = nil,
                     okButtonTitle: String? = nil,
                     okAction: (() -> Void)? = nil,
                     cancelButtonTitle: String? = nil,
                     cancelAction: (() -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: okButtonTitle ?? NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { (alert: UIAlertAction!) in
            okAction?()
        }))
        
        if cancelButtonTitle != nil {
            alert.addAction(UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { (alert: UIAlertAction!) in
                cancelAction?()
            }))
        }
        
        present(alert, animated: true, completion: nil)
    }
}
