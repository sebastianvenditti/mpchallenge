//
//  MPChallengeTests.swift
//  MPChallengeTests
//
//  Created by Sebastián Venditti on 25/03/2018.
//  Copyright © 2018 MercadoPago. All rights reserved.
//

import XCTest
@testable import MPChallenge

class MPChallengeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testTruncatedDecimalString() {
        XCTAssertEqual(Decimal(123_456.78).truncatedDecimalString(),
                       "123456.78",
                       "Amount formatted incorrectly")
        
        XCTAssertEqual(Decimal(0.789).truncatedDecimalString(),
                       "0.79",
                       "Amount formatted incorrectly")
        
        XCTAssertEqual(Decimal(123.0).truncatedDecimalString(),
                       "123.00",
                       "Amount formatted incorrectly")
        
        XCTAssertEqual(Decimal(123_456).truncatedDecimalString(),
                       "123456.00",
                       "Amount formatted incorrectly")
        
        XCTAssertEqual(Decimal(0.00078).truncatedDecimalString(),
                       "0.00",
                       "Amount formatted incorrectly")
    }
    
    func testLocalizedCurrencyString() {
        XCTAssertEqual(Decimal(123_456.78).localizedCurrencyString(),
                       "$123,456.78",
                       "Amount formatted incorrectly")
        
        XCTAssertEqual(Decimal(0.789).localizedCurrencyString(),
                       "$0.79",
                       "Amount formatted incorrectly")
        
        XCTAssertEqual(Decimal(123.0).localizedCurrencyString(),
                       "$123.00",
                       "Amount formatted incorrectly")
        
        XCTAssertEqual(Decimal(123_456).localizedCurrencyString(),
                       "$123,456.00",
                       "Amount formatted incorrectly")
        
        XCTAssertEqual(Decimal(0.00078).localizedCurrencyString(),
                       "$0.00",
                       "Amount formatted incorrectly")
    }
    
}
