
## Mercado Pago Challenge

Aplicación prototipo implementada como solución al challenge de Mercado Pago.

La arquitectura utilizada es Model-View-ViewModel (MVVM), que consiste en dejar en el VC lo relacionado a UIKit y separa lo demás en el VM (sin UI). Esto permite hacer tests sobre la lógica de los view models sin necesidad de instanciar vistas ni view controllers.


### Próximos pasos 

La siguiente es una lista de cosas que se podrían hacer para mejorar la solución, pero se dejaron de lado para simplificarla:

 - Agregar más tests unitarios.
 - Agregar tests de integración (UI).
 - Mostrar algo más amigable cuando no hay ítems en una lista.
 - Agregar internacionalización (por ahora, solo se dejó Español).
 - Mejorar la lógica de la primer pantalla para prevenir aún más que se ingresen valores inesperados.
 - Se podría hacer la selección del medio de pago, luego de la entidad emisora y por último de la financiación todo en una misma pantalla con secciones que se van colapsando.



